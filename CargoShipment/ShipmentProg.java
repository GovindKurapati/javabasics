package activity;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;



public class ShipmentProg {	
	
	public static void main(String[] args) {		
	
		
		Shipment s1=new Shipment(LocalDate.now(),LocalTime.now(),24);
		s1.CalculateDelivery();	
		
	}
}
enum publicholidays{
	NEWYEAR(LocalDate.of(LocalDate.now().getYear(), 01, 01)),
	REPUBLICDAY(LocalDate.of(LocalDate.now().getYear(), 01, 26)),
	INDEPENDENCEDAY(LocalDate.of(LocalDate.now().getYear(),8,15));
	
	private LocalDate ld;
	
	publicholidays(LocalDate ld) {
		this.ld=ld;
	}
	LocalDate getlocaldate() {
		return this.ld;
	}
}
class Shipment{
	
	
	LocalDate Shipmentdate;
	LocalTime time;
	float NoOfHours;
	LocalDate Deliverydate;
	LocalTime end= LocalTime.of(18,0,0);
	LocalDate deliveryDate;
	LocalTime deliveryTime;
	
	
	public Shipment(LocalDate d,LocalTime t,int hrs) {
		
		this.Shipmentdate=d;
		this.time=t;
		this.NoOfHours=hrs;
	}
	
	public void CalculateDelivery() {
		
		LocalDate d=Shipmentdate;
		LocalTime t=time;
		float h=NoOfHours;
		float mins=0;
		Duration dd = Duration.between(t, end);
		mins=((dd.toMinutes())-(dd.toHours() * 60));
		while(h>0) {
			d=CheckWorkingdayrnot(d);
			
			Duration duration = Duration.between(t, end);			
			
			t=LocalTime.of(6,0,0);
			
			if(h<=duration.toHours())
			{
				t=t.plus((long) h,ChronoUnit.HOURS);
			}
			h-=duration.toHours();		
			
			if(h>0)
			{
				d=d.plus(1,ChronoUnit.DAYS);					
			}
			
		}
		t=t.plusMinutes((long)mins);
		deliveryDate=d;
		deliveryTime=t;
		printValues();
		
	}
	
	public LocalDate CheckWorkingdayrnot(LocalDate d) {
		int t=d.getDayOfWeek().getValue();
		
		if(t==6 || t==7)
			d=d.plus(1,ChronoUnit.DAYS);	
		
		
		for(publicholidays ph:publicholidays.values()) {
			if(d.equals(ph.getlocaldate()))
				d=d.plus(1,ChronoUnit.DAYS);
		}
		return d;
	}
	
	public void printValues() {
		
		System.out.println("Ordered on  :"+Shipmentdate);
		System.out.println("NO.of hours for delivery  :"+ NoOfHours);
		System.out.println("Delivery date : "+deliveryDate);
		System.out.println("Delivery Time:\t" +deliveryTime);
			
	}
}

