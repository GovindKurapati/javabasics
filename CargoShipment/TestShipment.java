package activity;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;

public class TestShipment {
	
	@Test
	public void testHoliday() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String date = "2020-09-25";
		String arrivalDate = "2020-09-28";
		LocalDate localDate = LocalDate.parse(date, formatter);	
		LocalDate arrivalLocalDate=LocalDate.parse(arrivalDate, formatter);	
		
		Shipment s1=new Shipment(localDate,LocalTime.now(),24);
		s1.CalculateDelivery();
		
		assertEquals(s1.deliveryDate,arrivalLocalDate);
		
	}
	
	@Test
	public void testPublicHolidays() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String date = "2020-08-14";
		String arrivalDate = "2020-08-17";
		LocalDate localDate = LocalDate.parse(date, formatter);	
		LocalDate arrivalLocalDate=LocalDate.parse(arrivalDate, formatter);	
		
		Shipment s1=new Shipment(localDate,LocalTime.now(),24);
		s1.CalculateDelivery();
		
		assertEquals(s1.deliveryDate,arrivalLocalDate);
	}
	
	@Test
	public void testY2K() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String date = "1999-12-31";
		String arrivalDate = "2000-01-03";
		LocalDate localDate = LocalDate.parse(date, formatter);	
		LocalDate arrivalLocalDate=LocalDate.parse(arrivalDate, formatter);	
		
		Shipment s1=new Shipment(localDate,LocalTime.now(),24);
		s1.CalculateDelivery();
		
		assertEquals(s1.deliveryDate,arrivalLocalDate);
	}
	
	@Test
	public void testLeapYear() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String date = "2020-02-28";
		String arrivalDate = "2020-03-02";
		LocalDate localDate = LocalDate.parse(date, formatter);	
		LocalDate arrivalLocalDate=LocalDate.parse(arrivalDate, formatter);	
		
		Shipment s1=new Shipment(localDate,LocalTime.now(),24);
		s1.CalculateDelivery();
		
		assertEquals(s1.deliveryDate,arrivalLocalDate);
		
	}
	
	
	

}
