package day6.morning;

public class CustomExceptionDemo {
	public static void main(String[] args) {
		Child baby=new Child();
		Dog tiger=new Dog();
		Item item=new Stick();
		baby.playWithDog(tiger, item);
	}

}

class Child{
	public void playWithDog(Dog dog,Item item) {
		try{
			dog.play(item);
		}catch(DogExceptions de) {
			de.visit();
		}
	}
}
class Handler911{
	public void handle(DogBiteException dbe) {
		System.out.println("take him to hospital...."+dbe);
	}
	public void handle(DogBarkException dre) {
		System.out.println("no worries............just ignore..."+dre);
	}
	public void handle(DogHappyException dhe)
	{
		System.out.println("Dog is happy.."+dhe);
	}
}

class Dog{
	public void play(Item item) throws DogExceptions {
		item.execute();
	}
}

abstract class Item{
	public abstract void execute() throws DogExceptions;
}

class Stick extends Item{
	@Override
	public void execute() throws DogExceptions {
		throw new DogBiteException("You beat i bite..");		
	}
	
}

class Stone extends Item{
	@Override
	public void execute() throws DogExceptions  {
		throw new DogBarkException("You throw i bark..");		
	}
	
}

class Biscuit extends Item{
	@Override
	public void execute()throws DogExceptions  {
		throw new DogHappyException("I love biscuits..");		
	}
}


abstract class DogExceptions extends Exception{
	private static Handler911 h911;
	public static Handler911 getH911() {
		return h911;
	}
	static {
		h911 = new Handler911();
	}
	public abstract void visit();
	
	
}

class DogBiteException extends DogExceptions{
	private String msg;
	public DogBiteException(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return msg;
	}
	@Override
	public void visit() {
		getH911().handle(this);
		
	}
	
}

class DogBarkException extends DogExceptions{
	private String msg;
	public DogBarkException(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return msg;
	}
	@Override
	public void visit() {
		getH911().handle(this);
		
	}	
}


class DogHappyException extends DogExceptions{
	private String msg;
	public DogHappyException(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return msg;
	}
	@Override
	public void visit() {
		getH911().handle(this);
		
	}
}


