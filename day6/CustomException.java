package day6.morning;

import java.util.Scanner;

public class CustomException {
	public static void main(String[] args) {
		TestCustomException t= new TestCustomException();
		try {
			t.test();
		}
		catch(Throwable te)
		{
			System.out.println(te);
		}
		
	}

}

class TestCustomException{
	public void test() throws Throwable {
		//throws:
		//1.Skips the compile time check for exception handling.
		//2. It conveys to the caller that this method is capable of throwing an exception,
		//3.It ensures that the calling method handles or throws the exception
		//4. This scenario is called as handling - Checked Exeptions
		Scanner sc = new Scanner(System.in);
		System.out.println("Input value..");
		String in = sc.next();
		if(in.equals("ram"))
		{
			throw new MyException("This is custom exception message.."); //custom exception
		}
	}
}

class MyException extends Exception{
	String msg;
	public MyException(String msg) {
		this.msg= msg;
		
	}
	@Override
	public String toString() {
		return "The exception message is.."+msg;
	}
	
}
