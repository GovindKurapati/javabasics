package day6.morning;

public class SevaCenter {
	Services s[] = new Services[5];
	public SevaCenter() {
		for(int i=0;i<s.length;i++)
		{
			s[i]=new DummyServices();
		}
	}
	public void runService(int n)
	{
		s[n].execute();
	}
	
	public void setServices(int n,Services service)
	{
		s[n]=service;
	}	
	
}

abstract class Services{
	Hospital h;Police p;Corporation c;
	public Services() {
		init();
	}
	void init() {
		h=new Hospital();
		p=new Police();
		c=new Corporation();
	}
	abstract public void execute();
}

class DummyServices extends Services{
	@Override
	public void execute() {
		System.out.println("Yet to create a new service");
		
	}
}

class DeathCertificate extends Services{
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		System.out.println("Death Certificate process started..");	
		c.deathCertificate();
		h.postMortem();
		p.doInvestigation();
		System.out.println("Process ended..");
		
		
	}
}
