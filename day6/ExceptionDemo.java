package day6.morning;

public class ExceptionDemo {
	public static void main(String[] args) {
		System.out.println("Before Exception...");
		try {
			int a=1/0;			
		}
		catch(Exception e) {
			System.out.println(e);
		}
		finally {
			System.out.println("From finally block..");
		}
		System.out.println("After exception..");
		
		
	}

}
