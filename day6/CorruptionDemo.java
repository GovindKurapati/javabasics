package day6.morning;

public class CorruptionDemo {
	public static void main(String[] args) {
		DeathCertificate dc =new DeathCertificate();
		SevaCenter s = new SevaCenter();
		s.setServices(0, dc);
		s.runService(0);
	}

}

class Corporation{
	public void deathCertificate() {
		System.out.println("Death certificate is given,..");
	}
}

class Hospital{
	public void postMortem() {
		System.out.println("Do postmortem..");
	}
}

class Police{
	public void doInvestigation() {
		System.out.println("Police doing investigation..");
	}
}