package day2.noon;



public class SortingArray {

	public static void main(String[] args) {
		
		int arr[]= {3,2,7,1,4,6,5};
		
		SortingArray obj = new SortingArray();
		obj.ascendingsort(arr);
		obj.descendingsort(arr);
		
		
	}
	void ascendingsort(int a[])
	{
		for(int i=0;i<(a.length)-1;i++)
		{
			for(int j=i+1;j<a.length;j++)
			{
				if(a[j]<a[i])
				{
					int t=a[i];
					a[i]=a[j];
					a[j]=t;
				}
			}
		}
		System.out.print("Ascending Order ");
		for(int i=0;i<a.length;i++)
		{
			System.out.print(a[i]+" ");
		}
	}
	void descendingsort(int a[])
	{
		for(int i=0;i<(a.length)-1;i++)
		{
			for(int j=i+1;j<a.length;j++)
			{
				if(a[j]>a[i])
				{
					int t=a[i];
					a[i]=a[j];
					a[j]=t;
				}
			}
		}
		System.out.print("\nDescending Order ");
		for(int i=0;i<a.length;i++)
		{
			System.out.print(a[i]+" ");
		}
	}

}
