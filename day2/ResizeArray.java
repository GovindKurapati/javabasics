package day2.noon;

public class ResizeArray {
	public static void main(String[] args) {
		
		int n=5;
		int arr[]=new int[n];
		System.out.println("Number of elements before: "+arr.length);
		n=10;
		//arr=new int[n]; //re-initilising
		//Cannot be resized unless re-initialsed or making a copy of the same array.
		System.out.println("Number of elements after: "+arr.length);
	}

}
