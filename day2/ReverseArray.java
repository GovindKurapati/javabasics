package day2.noon;

public class ReverseArray {

	public static void main(String[] args) {
		
		int arr[]= {1,2,3,4,5};
		int l=arr.length-1;
		for(int i=0;i<arr.length/2;i++)
		{
			int t = arr[i];
			arr[i]=arr[l-i];
			arr[l-i]=t;
		}
		for(int i=0;i<=l;i++)
		{
			System.out.print(arr[i]+" ");
		}
	}

}
