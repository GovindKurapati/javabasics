package day2.noon;

public class StudentResult {
	public static void main(String[] args) {
		
		int arr [][]={{80,90,100},{60,70,80},{50,60,70}};
		for(int i=0;i<arr.length;i++)
		{
			int sum=0;
			for(int j=0;j<arr[i].length;j++)
			{
				sum+=arr[i][j];
			}
			System.out.println("Result of Student "+(i+1) +" is "+sum);
		}
	}
}
