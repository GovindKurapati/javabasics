package day2.noon;

public class PrimeCheck {

	public static void main(String[] args) {
		
		int n=100,f=0;
		for(int i=2;i<n/2;i++)
		{
			if(n%i==0)
			{
				f=1;
				break;
			}
		}
		if(n<2)
		{
			System.out.println("Not a Prime Number");
		}
		else
		{
			if(f==0)
			{
				System.out.println("Prime Number");
			}
			else {
				System.out.println("Not a Prime Number");
			}
		}

	}

}
