package day2.noon;

public class AverageCalc {

	public static void main(String[] args) {
		
		AverageCalc obj = new AverageCalc();
		obj.findAverage();

	}
	
	void findAverage() {
		int arr[] = {1,2,3,4,5};
		int sum=0;
		float average =0.0f;
		for(int i=0;i<arr.length;i++) {
			sum+=arr[i];
		}
		average = sum/arr.length;
		System.out.println("Average of "+arr.length+" numbers is = "+average);
		
	}

}
