package day3.noon;

public class ClassObjectDemo {
	public static void main(String[] args) {
		
		ReservationCounter central = new ReservationCounter();
		Customer1 ram =  new Customer1();
		System.out.println("money with ram before booking "+ram.money);		;
		central.bookticket(new ReservationSlip(), ram);
		System.out.println("money with ram before booking "+ram.money);

	}

}

class ReservationCounter{
	public void bookticket(ReservationSlip slip, Customer1 customer) {
		System.out.println("Ticket booking for the slip "+customer.money+"  is the money given");
		customer.money=customer.money-500;
	}
}

class Customer1{
	int money = 1000;
}

class ReservationSlip{
	
}
