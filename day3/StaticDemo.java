package day3.noon;

public class StaticDemo {
	public static void main(String[] args) {
		
		SingleTon s1 = SingleTon.createSingle();
		SingleTon s2 = SingleTon.createSingle();
		//even tough singleton is called two times it is made to create object only once..
		
		MultiTon m1 = new MultiTon();
		MultiTon m2 = new MultiTon();
		
	}
}

class SingleTon{
	private SingleTon() {
		System.out.println("Singleton object created...");
	}

	 private static SingleTon single;
	public static SingleTon createSingle() {
		if(single==null)
		{
			single=new SingleTon();
		}
		return single;
	}
	
}

class MultiTon{
	public MultiTon() {
		System.out.println("Multiton object created...");
	}

	
}

