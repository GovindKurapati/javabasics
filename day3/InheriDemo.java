package day3.noon;

//Inheritance Usage
//1.Polymorphic query
//2.Part whole Hierarchy
//3.Removal of if-else-if
//4.Object Reusability
//5.Code Reusability
public class InheriDemo {
	public static void main(String[] args) {
		
		PaintBrush brush = new PaintBrush();
		
		brush.paint = new PinkPaint();
		brush.doPaint();
	}

}

class PaintBrush{
	Paint paint;
	public void doPaint() {
		System.out.println(paint);
	}
	
}

class Paint{}
class RedPaint extends Paint{}
class BluePaint extends Paint{}
class GreenPaint extends Paint{}
class PinkPaint extends Paint{}

//This is called STRATEGY PATTERN
//formula to eliminate if else-if
//1.Delete if else-if
//2.Convert the condition to classes
//3.Group them under a hierarchy
//4.Create associations between class and hierarchy class.
