package day3.noon;

public class OverridingDemo {
	public static void main(String[] args) {
	
		Child ch = new Child();
		
		//Parent p = new Child()  //possible
		//Child ch = new Parent() //not possible
		ch.met();
	}

}

class Parent{
	Parent(){
		System.out.println("Parent constructor called..");
	}
	Parent(int i){
		System.out.println("Parent parameter constructor is called");
	}
	
	public void met() {
		System.out.println("Parent method is called");
	}
	
}

class Child extends Parent{
	Child(){
		
		super(7); // this must be first statement because parent must be initialized before anything in child.
		
		System.out.println("Child constructor called..");
	}
	
	public void met() {
		super.met();
		System.out.println("Child method is called");
	}
}

//visibility cannot be reduce
//return type must not be changed
//name and method parameters cannot be changed
