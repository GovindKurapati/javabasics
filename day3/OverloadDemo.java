package day3.noon;

//RULES

//The name of the method should be same
//The return type and access specifier can be different
//parameters passed must be compulsory different

public class OverloadDemo {
	public static void main(String[] args) {
		
		Help911 usehelp = new Help911();
		
		usehelp.help(new Medical("Heart Attack!!"));
		usehelp.help(new Theft("Theft Attack!!"));
		usehelp.help(new Domestic("Domestic Attack!!"));
		
	}
}



class Help911{
	
	public void help(Domestic d) {
		System.out.println("Domestic method invoked.."+d);		
	}
	
	public void help(Theft t) {
		System.out.println("Theft method invoked.."+t);		
	}
	
	public void help(Medical m) {
		System.out.println("Medical method invoked.."+m);		
	}	
}

class Theft{
	String msg;
	public Theft(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return "Theft Problem..."+this.msg;
	}
}

class Domestic{
	String msg;
	public Domestic(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return "Domestic Problem..."+this.msg;
	}
}

class Medical{
	String msg;
	public Medical(String msg) {
		this.msg=msg;
	}
	@Override
	public String toString() {
		return "Medical Problem..."+this.msg;
	}
}

