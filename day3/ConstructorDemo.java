package day3.noon;

public class ConstructorDemo {
	public static void main(String[] args) {
		
		Human govind = new Human();
		new Human("govindK"); //parameter constructor-called as VIRTUAL METHOD INVOCATION- POLYMORPHISM
		new Human(12);
		System.out.println(govind);
	}

}

class Human{
	public Human(){
//		this("Rahul"); //can call in this way too and again a new object is created:D
		System.out.println("No paramter constructor..");
		System.out.println("Human created...");
	}
	
	public Human(String s) {
		System.out.println("Paramter construction is invoked...");
	}
	
	public Human(int n) {
		System.out.println("Integer constructor is invoked having value as "+n);
	}
	
	//we are overriding the return message using the below statements.
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Human with hashCode=="+this.hashCode()+" is created having classNsme as "+getClass()+" and toString as "+super.toString();
	}
}
