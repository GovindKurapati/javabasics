package day3.noon;

public class ClassesDemo {
	public static void main(String[] args) {
		Bank bankname = new Bank();
		bankname.depositWork(bankname.kamal, bankname.c[1]);
		
	}

}

class Bank{
	String name = "IOB"; // single-data
	String activities[]= {"deposit","withdraw"}; //multiple-data
	BankManager kamal = new BankManager();//complex data type (data and behaviour)
	
	Customer ramu =  new Customer();
	Customer somu = new Customer();
	
	Customer c[] = {ramu,somu};
	public void depositWork(BankManager manager,Customer c)
	{
		manager.work1();
		c.cwork();
	}
	
}
class BankManager{
	String name = "Kamal";
	public void work1() {
		System.out.println("BankManager "+ name+ " work started.");
	}
}

class Customer{
	int accountNo;
	public void cwork() {
		System.out.println("Customer work started..");
	}
}