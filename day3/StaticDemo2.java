package day3.noon;

public class StaticDemo2 {
	public static void main(String[] args) {
		FlowerShop.sellFlower();
		FlowerShop.sellFlower();
		FlowerShop.sellFlower();
		FlowerShop.sellFlower();
		FlowerShop.met();
	}

}

class FlowerShop{
	private FlowerShop() {
		
	}
	static {
		System.out.println("Buy flowers..."); // acts as a constructor
	}
	
	public static void sellFlower() {
		System.out.println("Flowers sold...");
	}
	
	public static void met() {
		System.out.println("Method called..");
	}
}
