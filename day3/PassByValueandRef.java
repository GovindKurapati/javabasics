package day3.noon;

public class PassByValueandRef {
	public static void main(String[] args) {
		int value =100;
		System.out.println("Value now: "+ value);
		
		//pass by value
		PBV pbv  = new PBV();		
		pbv.acceptvalue(value);//only the value is now passed
		System.out.println("Value after passing  "+ value);
		
		//pass by reference
		PBR pbr = new PBR();		
		Inkpen pen = new Inkpen();
		System.out.println("Before passing.." + pen.ink);
		pbr.acceptobject(pen); //here the object is passed so changes are reflected..
		System.out.println("After passing.. "+pen.ink);
	}

}

class PBV{
	public void acceptvalue(int value) {
		value = 0;
	}
}

class PBR{
	public void acceptobject(Inkpen pen) {
		pen.ink = "half";
	}
}

class Inkpen{
	String ink = "Full!";
}
