package day9.morning;

public class OneObjectTwoThreadsOneJob {
	public static void main(String[] args) {
		Gun g = new Gun();
	
		new Thread(()->{
			for(int i=0;i<5;i++)
			g.fill();
		},"filler").start();
		

		new Thread(()->{
			for(int i=0;i<5;i++)
			g.shoot();
		},"shooter").start();
	}

}
//Inter-thread communication or Producer Consumer problem..

class Gun{
	boolean flag;
	
	synchronized public void fill() {
		if(flag)
		{
			try {wait();}catch(Exception e) {}; //wait() makes the current thread to wait to finish the exisitng thread to finish
		}
		System.out.println("Fill the gunn..");
		flag = true;
		notify();//notifies the waiting thread..
	}
	
	synchronized public void shoot() {
		if(!flag)
		{
			try {wait();}catch(Exception e) {};
		}
		System.out.println("Shoot the gunn..");
		flag = false;
		notify();
	}
}
