package day9.noon;

public class OneThreadOneObject {
	public static void main(String[] args) {
		
		new Thread(()->{
			Resource r = Factory.getResource(); //only one time object is created..
			r.name = "First Thread called..";
			Resource r2 = Factory.getResource();
			System.out.println(r.name);
			Factory.removeResourceFromThread();
		
			Resource r1 = Factory.getResource();
		}).start();
		
		new Thread(()->{
			Resource r = Factory.getResource();
			System.out.println("Second Thread called...");
			r = Factory.getResource();
		}).start();
		
	}
}

class Factory{
	private static ThreadLocal tlocal = new ThreadLocal();
	public static Resource getResource() {
		Resource r = (Resource)tlocal.get();
		if(r==null)
		{
			r=new Resource();
			tlocal.set(r);
			return r;
		}
		return r;		
	}
	
	public static void removeResourceFromThread() {
		if(tlocal.get()!=null)
		{
			tlocal.remove();
		}
	}
}

class Resource{
	String name;
	public Resource() {
		System.out.println("Resource constructor called..");
	}
}
