package day9.morning;

public class DeadLockDemo {
	public static void main(String[] args) {
		Frog frog = new Frog();
		Crane crane = new Crane();
		
		new Thread(()->{
			crane.eat(frog);
			
		}).start();
		
		new Thread(()->{
			frog.escape(crane);
			
		}).start();
		
	}
}

class Frog{
	synchronized public void escape(Crane c){
		c.craneLeaveMethod();
//		System.out.println(" freedom...");
	}
	synchronized public void frogLeaveMethod() {
		
	}
	
}

class Crane{
	synchronized public void eat(Frog f){
		System.out.println("  eating....");
		f.frogLeaveMethod();
		System.out.println("  swaha ....");
	}
	
	synchronized public void craneLeaveMethod() {
		
	}
}
