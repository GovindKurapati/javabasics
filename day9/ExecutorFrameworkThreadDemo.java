package day9.noon;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorFrameworkThreadDemo {
	public static void main(String[] args) throws Exception{
		
		ExecutorService es = Executors.newFixedThreadPool(3);
		
		es.execute(()->{
			System.out.println("Child thread 1..");
		});
		
//		es.execute(()->{
//			System.out.println("Chuld thread 2..");
//		});
		
		es.execute(new MyRunnableJob());
		
		Future future = es.submit(new MyCallableJob());
		
		System.out.println("Value returned.."+future.get());
		
		System.out.println("main thread..");
		es.shutdown();
		
			
		
		
	}

}


class MyRunnableJob implements Runnable{
	@Override
	public void run() {
		System.out.println("Thread called from runnable..");
		
	}
}

class MyCallableJob implements Callable{
	@Override
	public Object call() throws Exception {
		// TODO Auto-generated method stub
		return "Hello";
	}
	
}

