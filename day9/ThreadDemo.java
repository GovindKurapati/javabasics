package day9.morning;

public class ThreadDemo {
	public static void main(String[] args) {
		
		new Thread(()->{new Test().met();}).start();
		
		System.out.println("Second line..");
	}

}

class Test{
	public void met(){
		System.out.println("First line..");
		try {
			Thread.sleep(5000);
		}
		catch(Exception e) {}
	}
}
