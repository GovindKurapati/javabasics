package day9.morning;

public class OneObjectTwoThreads {
	public static void main(String[] args) {
		
		ReservationCounter counter = new ReservationCounter();
		
		new Thread(()->{
			synchronized(counter) { //object locking.. now only one thread runs at a time..
				counter.bookTicket(1000);
				counter.giveChange();
			}
		},"ramu").start();
		
		new Thread(()->{
			synchronized(counter) {
				counter.bookTicket(400);
				counter.giveChange();
//				counter.drinkWater();
			}
		},"somu").start();
		
	}
}



class ReservationCounter{
	int amt;
	 public void bookTicket(int amt)
	{
		this.amt = amt;
		Thread t = Thread.currentThread();
		String name = t.getName();
		System.out.println("Ticket booked for.."+name + "  and the amount paid is.."+amt);
	}
	
	 public void giveChange() {
		int change = amt-100;
		Thread t = Thread.currentThread();
		String name = t.getName();
		System.out.println("Change given to.."+name+"  and the change is .."+change);
	}
	public void drinkWater() {
		System.out.println("Drinking water...");
	}
}

