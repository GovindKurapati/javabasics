package day7.morning;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class InterfaceSubjectionDemo {
public static void main(String[] args) {
	
	AlopathyMedicalCollege stanley=new AlopathyMedicalCollege();
	AyurvedaMedicalCollege ayush=new AyurvedaMedicalCollege();
	JetAcademy jet=new JetAcademy();
	
	//Human gk=new Human();
	
	//SUBJECTION
	//Doctor doctorgk= (Doctor)Proxy.newProxyInstance(Human.class.getClassLoader(), new Class[] {Doctor.class}, new MyInvocationHandler(new Object[] {stanley}));
	
	//doctorgk.doCure();
	
	Object object=Proxy.newProxyInstance(Human.class.getClassLoader(), new Class[] {Doctor.class,Pilot.class},
			new MyInvocationHandler(new Object[] {stanley,jet}));
	
	
	
	Doctor doctorgk=(Doctor)object;
	Pilot pilotgk=(Pilot)object;
	
	doctorgk.doCure();
	
	pilotgk.fly();	
}
}
class MyInvocationHandler implements InvocationHandler{
	Object obj[];
	Object o;
	public MyInvocationHandler(Object obj[]) {
		this.obj = obj;
	}
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		for(int i=0;i<obj.length;i++)
		{
			try {
				o=method.invoke(obj[i], args);
			}
			catch(Exception e){}
		}
		return o;
	}
		
}



interface Doctor{
	public void doCure();
	public void doGiveMedicine();
}

interface Nurse{
	public void nursing();
}

interface Pilot{
	public void fly();
}

interface Steward{
	public void serve();
}

class AlopathyMedicalCollege implements Doctor,Nurse{
	@Override
	public void doCure() {
		System.out.println("Alopathy cure started..");
	}
	@Override
	public void doGiveMedicine() {
		System.out.println("Alopathy medicine ");
	}
	@Override
	public void nursing() {
		System.out.println("Nurse course..");
		
	}
}

class AyurvedaMedicalCollege implements Doctor{
	@Override
	public void doCure() {
		System.out.println("Ayurveda cure started..");
	}
	@Override
	public void doGiveMedicine() {
		System.out.println("Ayurveda medicine is given,...");
	}
}

class JetAcademy implements Pilot,Steward{
	@Override
	public void fly() {
		System.out.println("Learn to fly..");
		
	}
	@Override
	public void serve() {
		System.out.println("Learn to serve in fligts..");
	}
}

class Human{
	
}


//package day7.morning;
//import java.lang.reflect.InvocationHandler;
//import java.lang.reflect.Method;
//import java.lang.reflect.Proxy;
//public class InterfaceSubjectionDemo {
//public static void main(String[] args) {
//	AlopathyMedicalCollege stanley=new AlopathyMedicalCollege();
//	AyurvedaMedicalCollege ayush=new AyurvedaMedicalCollege();
//	JetAcademy jet=new JetAcademy();
//	
//	//Human shoiab=new Human();
//	//subjection
//	//Doctor doctorshoiab=
//		//	(Doctor)Proxy.newProxyInstance(Human.class.getClassLoader(), new Class[] {Doctor.class}, new MyInvocationHandler(new Object[] {stanley}));
//	
////	doctorshoiab.doCure();
//	
//	Object object=Proxy.newProxyInstance(Human.class.getClassLoader(), new Class[] {Doctor.class,Pilot.class},
//			new MyInvocationHandler(new Object[] {stanley,jet}));
//	
//	Doctor doctorshoiab=(Doctor)object;
//	Pilot pilotshoiab=(Pilot)object;
//	doctorshoiab.doCure();
//	
//	pilotshoiab.fly();	
//}
//}
//class MyInvocationHandler implements InvocationHandler{
//	Object obj[];
//	Object o;
//	public MyInvocationHandler(Object obj[]) {
//		this.obj=obj;
//	}
//	@Override
//	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//		for(int i=0;i<obj.length;i++) {
//			try {
//			 o=method.invoke(obj[i], args);
//			}catch(Exception e) {}
//		}
//		return o;
//	}
//}
//interface Doctor{
//	public void doCure();
//	public void doGiveMedicine();
//}
//interface Nurse{
//	public void nursing();
//}
//interface Pilot{
//	public void fly();
//}
//interface Steward{
//	public void serve();
//}
//class AlopathyMedicalCollege implements Doctor,Nurse{
//	@Override
//	public void doCure() {
//		System.out.println("alopathy cure for corona started...");
//	}
//	@Override
//	public void doGiveMedicine() {
//		System.out.println("no medicine in alopathy so give inji morabba....");
//	}
//	@Override
//	public void nursing() {
//		System.out.println("nursing course...........nurse work done...");
//	}
//}
//class AyurvedaMedicalCollege implements Doctor{
//	@Override
//	public void doCure() {
//		System.out.println("ayurved cure for corona started...");
//	}
//	@Override
//	public void doGiveMedicine() {
//		System.out.println("kabasura neer given to cure.......");
//	}
//}
//class JetAcademy implements Pilot,Steward{
//	@Override
//	public void fly() {
//		System.out.println("learning to fly aeroplane............");
//	}
//	@Override
//	public void serve() {
//		System.out.println("become a aeroplane velakaaran....");		
//	}
//}
//class Human{
//	
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
