package day7.noon;

public class CompositionDemo {
	public static void main(String[] args) {
		IceCream cream = new Vanilla(new Nuts(new Fruits(new Chocolate())));
		System.out.println("Total cost of the icecream.."+cream.cost());
	}
}

abstract class IceCream{
	public abstract int cost();
}

abstract class Cream extends IceCream{
	
}

class Vanilla extends Cream{
	IceCream cream;
	public Vanilla() {
		// TODO Auto-generated constructor stub
	}
	public Vanilla(IceCream cream) {
		this.cream = cream;
		// TODO Auto-generated constructor stub
	}
	@Override
	public int cost() {
		if(cream!=null)
		{
			return 10+cream.cost();
		}
		return 10;
	}
}

class StrawBerry extends Cream{
	IceCream cream;
	public StrawBerry() {
		// TODO Auto-generated constructor stub
	}
	public StrawBerry(IceCream cream) {
		this.cream = cream;
		// TODO Auto-generated constructor stub
	}
	@Override
	public int cost() {
		if(cream!=null)
		{
			return 15+cream.cost();
		}
		return 15;
	}
}
class Chocolate extends Cream{
	IceCream cream;
	public Chocolate() {
		// TODO Auto-generated constructor stub
	}
	public Chocolate(IceCream cream) {
		this.cream = cream;
		// TODO Auto-generated constructor stub
	}
	@Override
	public int cost() {
		if(cream!=null)
		{
			return 20+cream.cost();
		}
		return 20;
	}
}

abstract class Ingredients extends IceCream{
}

class Nuts extends Ingredients{
	IceCream cream;
	public Nuts() {
		// TODO Auto-generated constructor stub
	}
	public Nuts(IceCream cream) {
		this.cream=cream;
	}
	@Override
	public int cost() {
		if(cream!=null)
		{
			return 5+cream.cost();
		}
		return 5;
		
	}
}

class Fruits extends Ingredients{
	IceCream cream;
	public Fruits() {
		// TODO Auto-generated constructor stub
	}
	public Fruits(IceCream cream) {
		this.cream=cream;
	}
	@Override
	public int cost() {
		if(cream!=null)
		{
			return 10+cream.cost();
		}
		return 10;
		
	}
}


