package day7.noon;

public class PrototypeDemo {
	public static void main(String[] args) {
		
		//first scenario
		System.out.println("First scenario..two objects created.");
		
		Sheep s1 = new Sheep();
		Sheep s2 = new Sheep();
		s1.name="Original";
		s2.name="Duplicate";
		System.out.println(s1.name+":"+s2.name);
		
		//second scenario
		
		System.out.println("Second scenario..one object with two refernces.");
		Sheep sheep1 = new Sheep();
		Sheep sheep2 = sheep1;
		
		sheep1.name="Original";
		sheep2.name = "Duplicate";
		System.out.println(sheep1.name+":"+sheep2.name);

		//third scenario
		System.out.println("Third scenario....clone/proto-Resouces are shared but properties are unique..one object with two references but data is unique");
		Sheep mother = new Sheep();
		Sheep dolly = mother.createProto();
		
		mother.name = "I am Mother sheep";
		dolly.name="I am the clone..dollyy";
		System.out.println(mother.name +":"+dolly.name);
		
		
	}
	

}

class Sheep implements Cloneable{
	String name;
	public Sheep() {
		System.out.println("Sheep constructor is called.");
	}
	
	
	public Sheep createProto(){
		try {
			return (Sheep)super.clone();
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
			
		}
	}
}
