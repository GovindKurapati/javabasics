package day7.noon;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;

public class GCDemo {
	public static void main(String[] args) {
		
		Runtime r = Runtime.getRuntime() ;
		System.out.println("Before object birth.."+r.freeMemory());
		
		GrandFather gf = new GrandFather();
		System.out.println("After object birth.."+r.freeMemory());
		
//		SoftReference<GrandFather> soft = new SoftReference<GrandFather>(gf); //even after object is dead its reference is gained again...BENCh period
		
		WeakReference<GrandFather> weak = new WeakReference<GrandFather>(gf); 
		gf=null; //In reality it should not be made null
		
		System.out.println("After object death.."+r.freeMemory());
		System.out.println("Doing rituals..");
		r.gc();//in reality we dont call gc, rather it is called by JVM-automatically
		System.out.println("After rituals.."+r.freeMemory());
		
		gf= weak.get();
//		gf=soft.get();//even after object is nulled its value is re-returned
		System.out.println(gf.age);
		
	}

}

class GrandFather{
	String age;
	private String gold ="Under the tree.";
	public GrandFather() {
		for(int i=0;i<1000;i++)
		{
			age = new String(i+"");
		}
	}
	private String getGold() {
		return "The gold is .."+gold;
	}
	@Override
	protected void finalize() throws Throwable {
		System.out.println("Finalized called.."+getGold());
	}
}
