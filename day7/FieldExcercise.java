package day7.morning;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FieldExcercise {
	public static void main(String[] args) throws Exception {
		FindShapeArea fs = new FindShapeArea();
		Square s = new Square();
		Rectangle r = new Rectangle();
		fs.findArea(r);
	}

}

class FindShapeArea{
	public void findArea(Object o) throws Exception {
		Class c = o.getClass();
		Field field = c.getField("side");
		System.out.println(field.get(o));
		
		Method met = c.getMethod("area");
		met.invoke(o);
		
		revealSecret(o);
	}
	
	public void revealSecret(Object o) throws Exception
	{
		Class c= o.getClass();
		Field field = c.getDeclaredField("secretSide");
		field.setAccessible(true);
		System.out.println(field.get(o));
		
		Method met = c.getDeclaredMethod("secretArea");
		met.setAccessible(true);
		met.invoke(o);
	}
}

abstract class Shape{
	
}

class Square extends Shape{
	public int side = 4;
	private int secretSide = 5;
	public void area() {
		System.out.println("Area of square is.."+4*side);
	}
	private void secretArea() {
		System.out.println("Secret Area of square iss.."+4*secretSide);
	}
}

class Rectangle extends Shape{
	public int side = 2;
	
	public void area() {
		System.out.println("Area of rectangle is.."+side*3);
	}
	private int secretSide=3;
	private void secretArea() {
		System.out.println("Secret Area of rectangle is.."+secretSide*4);
	}
	
}
