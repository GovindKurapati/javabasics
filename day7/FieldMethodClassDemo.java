package day7.morning;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FieldMethodClassDemo {
	public static void main(String[] args)throws Exception {
		PoliceStation p1 = new PoliceStation();
		Politician ramu = new Politician();
		Naxalite n = new Naxalite();
		p1.arrest(ramu);
	}
}

class PoliceStation{
	public void arrest(Object p) throws Exception {
		
		//introspection.. or Reflection
		Class c=p.getClass();
		Field field=c.getField("name");
		System.out.println(field.get(p));//to get specific field name
		
		Field fields[] = c.getFields(); //to get all fields and store in array
		for(Field f:fields) {
			System.out.println(f.getName());
		}
		
		Method met = c.getMethod("work", new Class[] {String.class}); //for naxalite method work
		met.invoke(p, new Object[] {"Hello World"});		
		
		Method methods[] = c.getMethods();//to get all methods of that object
		for(Method m:methods) {
			System.out.println(m.getName());			
		}
		tortureRoom(p); // reveals all private fields and methods..
	}
	public void tortureRoom(Object p) throws Exception{
		Class c = p.getClass();
		Field field = c.getDeclaredField("secretName");
		field.setAccessible(true);
		System.out.println(field.get(p)); //to access private fields
		
		Method met = c.getDeclaredMethod("secretWork", new Class[] {String.class});
		met.setAccessible(true);
		met.invoke(p, new Object[] {"secret work revealed.."});
	}
	
}

class Naxalite{
	public String name="I am naxalite";
	public void work(String s) {
		System.out.println("The work.."+s);
	}
}

class Politician{
	public String name="I am politician";
	private String secretName="dash  dash..";
	
	public void work(String s) {
		System.out.println("I do social work.."+s);
	}
	
	private void secretWork(String s) {
		System.out.println("Dash dash..."+s);
	}
}