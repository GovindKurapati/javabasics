package day8.morning;

public class MergingInnerClassDemo {
	public static void main(String[] args) {
		Pepsii pepsico = new Pepsii();
		KaliMarkk kali = new KaliMarkk();
		pepsico.makePepsi();
		kali.makeBovonto();
	}

}

abstract class Colaa{ //Functional abstract (only one method) -make use of annonymous inner class
	public abstract void makeCola();
}

interface ColaInter{//Functional Interface - Lambda Syntax..
	public void makeColaa();
}

interface ColaInter2{
	public void makeCola(String s,int i);
}



class Pepsii{
	public void makePepsi() {
		new Colaa(){ //anonymous inner class
			@Override
					public void makeCola() {
						System.out.println("Make cola...");
						
					}		
		
		}.makeCola(); //no class..completely campacola is merged with pepsi and Kalimark cannot access it.
		//only one method can be overridden and called.
		
		System.out.println("Fill in pepsi botlle and sell...");
		
		//Lambda Syntax
		ColaInter c1 =()->{
			System.out.println("From colainter inerface..");
		};
		c1.makeColaa();
		
		ColaInter2 c2 = (value,num)->{
			System.out.println("The value is.."+value);
			System.out.println("The number is.."+num);
		};
		c2.makeCola("aaa",23);
		
		Add add=(i,j)->{
			return i+j;
		};
		addNumber(10,20,add);
		
		
	}
	public void addNumber(int i,int j,Add add)
	{
		System.out.println("Addition of i and j is.."+add.add(i,j));
	}
}

interface Add{
	public int add(int i,int j);
}

class KaliMarkk{
	public void makeBovonto(){
		//Cola cola = new Pepsi().trojan(); //cannot access CampaCola
		//cola.makeCola();
		System.out.println("Fill in bovonto bottle and sell..");
	}
}
