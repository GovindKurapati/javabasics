package day8.noon;

public class MultiThreading {
	public static void main(String[] args) throws Exception {
		Thread t = Thread.currentThread();
		t.setPriority(10);
		t.setName("gk");
		System.out.println(t);
		for(int i=0;i<5;i++)
		{
			System.out.println(i);
			Thread.sleep(1000);
			System.exit(1);//to stop on abnormal condition
		}
		
		//to span a new thread..
		Thread t2 = new Thread(()->{System.out.println("Child thread..");});//using lambdas--first priority
		
		Thread t3 = new Thread(new MyRunnable()); //traditional -third 
		
		new Thread(new Runnable() { // annoynmous inner class -second
			@Override
			public void run() {
				System.out.println("Child Thread....");
				
			}
		}).run();
		
		
	}
	

}

class MyRunnable implements Runnable{
	@Override
	public void run() {
		System.out.println("Chuld thread...");
		
	}
}
