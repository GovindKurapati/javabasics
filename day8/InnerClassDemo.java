package day8.morning;

public class InnerClassDemo {
	public static void main(String[] args) {
		Pepsi pepsico = new Pepsi();
		KaliMark kali = new KaliMark();
		pepsico.makePepsi();
		kali.makeBovonto();
	}

}

abstract class Cola{
	public abstract void makeCola();
}



class Pepsi{
	public void makePepsi() {
		class CampaCola extends Cola{ //local inner class
			@Override
			public void makeCola() {
				System.out.println("Cola made by campacola..");
			}
		}
		
	
		Cola cola = new CampaCola();	
		cola.makeCola();
		System.out.println("Fill in pepsi botlle and sell...");
	}
	
//	public void trojan() {
//		return new CampaCola();
//	}
}

class KaliMark{
	public void makeBovonto(){
		//Cola cola = new Pepsi().trojan(); //cannot access CampaCola
		//cola.makeCola();
		System.out.println("Fill in bovonto bottle and sell..");
	}
}
