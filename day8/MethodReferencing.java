package day8.morning;

public class MethodReferencing {
	public static void main(String[] args) {
		Func1 fadd = new AdBazzar()::add; //new object created and then reference made
		int sum1 = fadd.add(10,20);
		System.out.println("Sum1 is..."+sum1);
		
		
		Func1 fstatic = AdBazzar::addStatic; //method referencing //no new keyword..already things are there we need to just implement..
		int sum2 = fstatic.add(20, 30);
		System.out.println("Sum2 is..."+sum2);
		
		
	}
}

interface Func1{
	public int add(int i,int j);
}

class AdBazzar{
	public int add(int i,int j)
	{
		return i+j;
	}
	
	public static int addStatic(int i,int j)
	{
		return i+j;
	}
}