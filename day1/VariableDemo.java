package day2.noon;

public class VariableDemo {
	
	public static void main(String[] args) {
		
		String s1= new String("Hello");
		String s2="Hello Java";
		
		char c1='a';
		Character c2=new Character('b');//These type of declaration is called WRAPPER class

		
		int n1=10;
		Integer n2= new Integer(20);
		
		float f1= 1.23f;
		Float f2 = new Float(2.34f);
		
		double d1 = 1.234566;
		Double d2= new Double(2.3445);
		
		boolean b1 = true;
		Boolean b2= Boolean.TRUE;
		
		//String to integer
		String s3="2020";
		int data=Integer.parseInt(s3);
		
		
		System.out.println(data);
		
		
		System.out.printf("%-12s%-12s%s","Column 1","Column 2","column 3");
		System.out.printf("%-12.5s%s","Hello World","world");
	}

}
