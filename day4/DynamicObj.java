package day4.morning;

public class DynamicObj {
	public static void main(String[] args) throws Exception {
		Class c = Class.forName("day4.morning.Test");
		Test t=(Test)c.newInstance();
		t.work();
		
		//or
		//Object o = c.newInstance();
		//((Test)o).methodname();
		
	}
	
	

}

class Test{
	public Test() {
		System.out.println("Test object created..");
	}
	public void work() {
		System.out.println("Work method called..");
	}
}