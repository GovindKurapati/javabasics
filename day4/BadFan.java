package day4.morning;

import java.util.Scanner;

public class BadFan {
	public static void main(String[] args) throws Exception {
		Badfann b  = new Badfann();
		Scanner sc = new Scanner(System.in);
		while(true)
		{
			System.out.println("Enter the pull method..");
			sc.next();
			b.pull();
		}
		
	}

}

class Badfann{
	int state= 0 ;
	public void pull() {
		if(state == 0)
		{
			System.out.println("Switch on state..");
			state=1;
		}
		else if(state==1)
		{
			System.out.println("Medium speed state");
			state=2;
		}
		else if(state==2)
		{
			System.out.println("High speed");
			state=3;
		}
		else if(state ==3)
		{
			System.out.println("Switch off state..");
			state=0;
		}
	}
}

