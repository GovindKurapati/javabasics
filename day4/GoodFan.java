package day4.morning;

import java.util.Scanner;

public class GoodFan {
	public static void main(String[] args) {
		
		Goodfann g = new Goodfann();
		Scanner sc = new Scanner(System.in);
		while(true)
		{
			System.out.println("Enter the pull method..");
			sc.next();
			g.pull();
		}
		
		
	}

}

class Goodfann{
	State state = new SwitchOffState();
	public void pull() {
		state.execute(this);
	}
	
}
//Bidirectional association
//Object state management
abstract class State{
	public abstract void execute(Goodfann f);	
}

class SwitchOffState extends State{
	
	public void execute(Goodfann f) {
		System.out.println("Switch on state..");
		f.state= new MediumSpeedState();
	}		

}

class MediumSpeedState extends State{
	
	public void execute(Goodfann f) {
		System.out.println("Medium speed state on..");
		f.state= new HighSpeedState();
	}		

}

class HighSpeedState extends State{
	
	public void execute(Goodfann f) {
		System.out.println("High spped on state..");
		f.state= new SwitchOffState();
	}		

}


	



