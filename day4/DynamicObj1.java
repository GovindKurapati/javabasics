package day4.morning;

import java.lang.reflect.Constructor;

public class DynamicObj1 {
	public static void main(String[] args) throws Exception {
		
		Constructor c =Class.forName("day4.morning.Test1").getConstructor(String.class);
		TestParent o = (TestParent)c.newInstance("Hello");
		o.work();
	}

}

class TestParent{
	public void work() {}
}

class Test1 extends TestParent{
	public Test1(String s) {
		System.out.println("Test1 constructor called.."+s);
	}
	public void work() {
		System.out.println("Work method in Test1 called..");
	}
}


