package day4.morning;

import java.util.Scanner;

public class SolutionDog {
	public static void main(String[] args) throws Exception{
		
		Dog1 tiger = new Dog1();
		Child1 baby = new Child1();
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Please enter a item...");
		
		String itemClass = scan.next();
		Item item = (Item)Class.forName(itemClass).newInstance();
		baby.playwithdog(tiger,item);
	}

}

class Dog1{
	public void play(Item item)
	{
		item.execute();
	}
}

abstract class Item {
	public abstract void execute();
}

class Stick1 extends Item{
	@Override
	public void execute() {
		System.out.println("You hit i will bite..");
	}
}

class Stone1 extends Item{
	@Override
	public void execute() {
		System.out.println("You throw i will bite..");
	}
}
class Child1{
	public void playwithdog(Dog1 dog,Item item) {
		dog.play(item);
	}
}

