package day4.morning;

public class AggregationDemo {
	public static void main(String[] args) {
		ShakthiSocket ss = new ShakthiSocket();
//		ShakthiPlug sp = new ShakthiPlug(); //Indian pin working
//		ss.roundPinHole(sp);//indian pin working
		HPPlug hp = new HPPlug(); //american pin working..
		IndianAdapter ia = new IndianAdapter(); //american pin working..
		ia.ap=hp; //american pin working..
		ss.roundPinHole(new ShakthiPlug()); //indian pin working..
		ss.roundPinHole(ia); //american pin working...
 	}

}


abstract class IndianPlug{
	public abstract void roundPin();
}

class ShakthiPlug extends IndianPlug{
	@Override
	public void roundPin() {
		System.out.println("Indian pin working...");		
	}
}

abstract class IndianSocket{
	public abstract void roundPinHole(IndianPlug ip);
}

class ShakthiSocket extends IndianSocket{
	@Override
	public void roundPinHole(IndianPlug ip) {
		ip.roundPin();
	}
}

abstract class AmericanPlug{
	public abstract void slabPin();
}

class HPPlug extends AmericanPlug{
	@Override
	public void slabPin() {
		 System.out.println("American pin working..");
		
	}
}

class IndianAdapter extends IndianPlug{
	AmericanPlug ap; //aggregation behaviour where american plug is now become a PART OF india plug
	@Override
	public void roundPin() {
		ap.slabPin();		
	}
}