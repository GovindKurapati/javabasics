package day10.morning;

import java.util.ArrayList;

public class GenericsDemo {
	public static void main(String[] args) {
		PaintBrush<Paint> pb=new PaintBrush<>();
		pb.setObj(new RedPaint());
		Paint paint=pb.getObj();
		paint.doColor();
		
		ArrayList<Integer> al=new ArrayList<>();
		al.add(3444);
		al.add(2333);
		al.add(1223);
		for(int i:al) {
			System.out.println(i);
		}
	}

}

class PaintBrush<T>{
	private T obj;

	public T getObj() {
		return obj;
	}

	public void setObj(T obj) {
		this.obj = obj;
	}
	
}

abstract class Paint{
	abstract void doColor();
}

class RedPaint extends Paint{
	@Override
	void doColor() {
		System.out.println("Res color..");
	}
}

abstract class Liquid{
	abstract void spray();
}

class ColourLiquid extends Liquid{
	@Override
	void spray() {
		System.out.println("holi  holi...");
		
	}
}

abstract class Dry{
	abstract void dusting();
}

class DryAir extends Dry{
	@Override
	void dusting() {
		System.out.println("I clean my laptop...");
		
	}
}
