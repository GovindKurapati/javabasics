package day10.morning;

import java.util.Optional;


public class OptionalDemo {
public static void main(String[] args) {
	
	Optional <String> o1=Optional.of("hari");
	System.out.println(o1);
	
	Optional <String> o2=Optional.ofNullable(null);
	System.out.println(o2);
	System.out.println(o1.equals(o2));
	
	Optional <Object> o3=Optional.empty();
	System.out.println(o3);
	
	//hash code
	System.out.println("Hashcode is "+o1.hashCode());
	System.out.println("Hashcode is "+o2.hashCode());
	
	//orElse
	String name="aaaaa";
	String name1=Optional.ofNullable(name).orElse("aaa"); 
	
	System.out.println(name1);
	//orElseGet
	String name2=Optional.ofNullable(name).orElseGet(()->"ccc");
	
	System.out.println(name2);
	//orElseThrow
	String name3=Optional.ofNullable(name).orElseThrow(IllegalArgumentException::new);
	
	//filter
	int year=2016;
	Optional<Integer> yearOptional=Optional.of(year);
	boolean is2016=yearOptional.filter(y->y==2016).isPresent();
	boolean is2017=yearOptional.filter(y->y==2017).isPresent();
	System.out.println(is2016);
	System.out.println(is2017);
	
	//map
	String name4="abcdef";
	Optional <String > nameOptional=Optional.of(name4);
	int len=nameOptional.map(String::length).orElse(0);
		System.out.println(len);
		
		//get 
		Optional <String> getm=Optional.ofNullable("qwerty");
		
		String name5=getm.get();
		System.out.println(name5);
		
		//tostring
		Optional <Integer> op=Optional.of(4231);
		String v1=op.toString();
		System.out.println(op);
		
		
}
}
class Optional2{
	public Optional<String> getEmpty(){
		return Optional.empty();
	}
}