package day10.morning;

public class AssertionDemo {
	public static void main(String[] args) {
		
		int a=11;
		
//		assert a==10; //simple expression assert (boolean returning condition);
		
//		assert a==10 : "The value is not equal to 10";
//		assert a==10 : met();
		
		
		try {
			assert a==10 : ++a;			
		}
		catch(AssertionError e)
		{}
//		assertEquals(1, 2);
//		assertArrayrEquals(e,a);
		
		System.out.println("The value of a=="+a);
	}
	
	public static String  met() {
		return "The value is not 10";
	}

}
